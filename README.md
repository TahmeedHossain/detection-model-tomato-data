# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Plant disease detection model ###

This repository includes the detection model which will help to detect diseases for tomato dataset with 93% accuracy.
More accuracy is possible with higher epoch/ number of images. 
The model is a CNN model and hyperparameters of the model are:

* EPOCHS = 40
* STEPS = 100
* LR = 1e-3
* BATCH_SIZE = 32
* WIDTH = 256
* HEIGHT = 256
* DEPTH = 3

Train test ratio - .8/.2  

Total class - 10  

Number of images used to train the model - 1000 from each class  

Total number of images - 8857 , some of the class doesn't have 1000 images. 

### Setup ###

Virtual environment can be automatically created from Conda. But in order to create virtual environment manually follow this

1. Download and install conda 
2. ``` conda install jupyter```  - to install jupyter on conda 
3. ```conda create -n Plant_disease_Model python=3.8 ```  - to create virtual environment
4. ```source activate Plant_disease_Mode ``` - to activate the environment 
5. ``` pip install -r requirements.txt ``` to install the packages
5. ``` jupyter notebook ```- to open jupyter
